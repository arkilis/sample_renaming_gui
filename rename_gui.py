import os
import Tkinter
import tkMessageBox
from tkFileDialog import askdirectory 
from Tkinter import *

path= ""

def rename(dir, text):
    print (dir, text)
    for e in os.listdir(dir):
        if(os.path.isfile(os.path.join(dir,e))):
            ary= e.split("_")
            l= len(ary)

            first_part  = "_".join(ary[:-1])
            last_part   = ".".join(ary[-1].split(".")[1:])
            new_name    = first_part+"."+last_part
            os.rename(os.path.join(dir,e), os.path.join(dir,new_name))
            print os.path.join(dir,e)
            text.insert(END, "renaming "+new_name+"\n")

def selectDir(label):
    global path
    path= askdirectory()
    label.config(text=path)
    
def process(text):
    global path
    if(path==""):
        tkMessageBox.showinfo("Warning", "Please select a plate directory first.")
    else:
        rename(path, text)
        msg= "Please double check the sample names.\n "
        msg+= "Any issue please contact Ben.liu@agrf.org.au"
        tkMessageBox.showinfo("Finished", msg)

win = Frame()
win.master.title("AGRF Sample Rename @ 2016")

win.grid(sticky=N+S+E+W)

frame_top = LabelFrame(win, text='Choose Dir', padx=5, pady=5)
frame_btm = LabelFrame(win, text='Output',     padx=5, pady=5)
frame_top.grid(sticky=E+W)
frame_btm.grid(sticky=E+W)

for frame in frame_top, frame_btm:
    for col in 0, 1, 2:
        frame.columnconfigure(col, weight=1)

# set up label
Tkinter.Label(win,  text='Choose Plate Directory: ').grid(in_=frame_top, sticky=W)
# set up label
label= Tkinter.Label(win)
label.grid(in_=frame_top, row=0, column=1, sticky=W)
# set up select button
Tkinter.Button(win, text='Select', command= lambda:selectDir(label)).grid(in_=frame_top, row=0, column=2, sticky=E)

# set up ouput
text= Tkinter.Text(win, width=70, height=20)
text.grid(in_=frame_btm, row=0, column=1, sticky=W)

# set up run button
Tkinter.Button(win, text='Process', command= lambda: process(text)).grid(in_=frame_top, row=0, column=3, sticky=E)

    
win.mainloop()





